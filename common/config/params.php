<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'backendUrl' => 'http://localhost/e_perpus/backend/web/upload/'
];
