<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Item;
use frontend\models\ItemSearch;
use frontend\models\ItemCategory;
use frontend\models\Orders;
use frontend\models\OrdersItem;
use frontend\models\Customer;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yz\shoppingcart\ShoppingCart;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'update', 'delete', 'add-to-cart'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['checkout', 'history'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $q = $searchModel->search(Yii::$app->request->queryParams);
        $count = $q->count();

        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 4]);

        $model = $q->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();

        $data = ItemCategory::find()->all();

        return $this->render('index',[
            'data' => $data,
            'searchModel' => $searchModel,
            'model' => $model,
            'pagination' => $pagination,
        ]);
    }

    public function actionAddToCart($id)
    {
        $cart = new ShoppingCart();
        $model = Item::findOne($id);
        if ($model){
            $cart->put($model, 1);
            $data = $cart->getPositions();
            return $this->render('cart',[
                'data' => $data,
            ]);
        }
        throw new NotFoundHttpException;
    }

    public function actionCheckout(){
        $cart = new ShoppingCart();
        $morder = new Orders();

        $cusid = Customer::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();

            $morder->date = date('Y-m-d H:i:s');
            $morder->customer_id = $cusid->id;

            $morder->save();

            foreach ($cart->getPositions() as $data) {
                $mdetorder = new OrdersItem();
                $mdetorder->order_id = $morder->id;
                $mdetorder->item_id = $data->id;
                $mdetorder->save();
            }

            $cart->removeAll();

            return $this->redirect(['history']);
    }

    public function actionHistory(){
        $cusid = Customer::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();

        $data = Orders::find()->where(['customer_id'=>$cusid->id])->all();

        return $this->render('history',[
            'data'=>$data,
        ]);
    }

    public function actionHisdet($id){
        $data = OrdersItem::find()->where(['order_id'=>$id])->all();

        return $this->render('hisdet',[
            'data'=>$data,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
