<?php
	
	use yii\helpers\Html;

	$this->title = 'History';
	$this->params['breadcrumbs'][] = $this->title;

?>

<div class="item-index">
	<table>
		<thead>
			<tr>
				<td>ID</td>
				<td>Tanggal</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data as $row) { ?>
				<tr>
					<td><?= $row->id ?></td>
					<td><?= $row->date ?></td>
					<td><?= Html::a('detail',['hisdet','id' => $row->id]) ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>