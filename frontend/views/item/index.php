<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Buku';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'data' => $data]); ?>

    <?php foreach ($model as $row) { ?>
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
            <div class="product-image-wrapper baru" style="margin-bottom: 10px;">
                <div class="single-products">
                    <div class="productinfo text-center" style="margin-bottom: 10px; margin-top: 10px;">
                        <?= Html::a(Html::img(Yii::$app->params['backendUrl'] . $row->img, ['alt'=>'yii','style' =>'width: 90%;']), [ 'detail', 'id' => $row->id],['style' =>'max-width: 100%;']) ?>﻿
                        <!-- <h3>Rp <?= number_format($row->price,0,",",".") ?></h3> -->
                        <p><h3><?= $row->name; ?></h3></p>

                        <?= Html::a('Pinjam', ['add-to-cart','id' => $row->id], ['class' => 'btn btn-success']) ?>
                        <?= Html::a('Baca', ['view','id' => $row->id], ['class' => 'btn btn-info']) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?= LinkPager::widget([
    'pagination' => $pagination,
    ])?>
</div>
